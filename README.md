# Zappa

Zappa is a freelancer site.  This repo is the backend part of the website.
This is a simple API for CRUD operations.

## Technology

Node.js, Express.js, MongoDB

## Deploy

http://zappa-frontend.herokuapp.com/home