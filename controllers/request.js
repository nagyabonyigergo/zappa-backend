const Job = require("../models/job");
const User = require("../models/user");
const Request = require("../models/request");

exports.getRequests = async (req, res, next) => {
  const userId = req.userId;
  try {
    const user = await User.findById(userId);
    const request = await Request.find({ _id: user.requests })
      .populate("creator")
      .populate("job");
    res.status(200).json({
      message: "Fetched request successfully.",
      request: request,
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.createRequest = async (req, res, next) => {
  const userId = req.userId;
  const jobId = req.body.jobId;
  const job = await Job.findById(jobId);
  const user = await User.findById(job.creator._id).populate("requests");
  const requests = await Request.find();
  const exsistingRequest = requests.find(request => request.job.toString() === jobId && request.creator.toString() === userId );
  try {
    if (exsistingRequest) {
      const error = new Error("Existing request.");
      throw error;
    }
    const request = new Request({
      creator: req.userId,
      job: jobId,
    });
    await request.save();
    user.requests.push(request);
    await user.save();
    res.status(201).json({
      message: "Request created successfully!",
      request: request,
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.getRequestById = async (req, res, next) => {
  const requestId = req.params.id;
  const request = await Request.findById(requestId)
    .populate("creator")
    .populate({ path: "job", populate: { path: "creator" } });
  try {
    if (!request) {
      const error = new Error("Could not find request.");
      error.statusCode = 404;
      throw error;
    }
    res.status(200).json({ message: "request fetched.", request: request });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.deleteRequest = async (req, res, next) => {
  const requestId = req.params.id;
  try {
    const request = await Request.findById(requestId).populate('job');
    if (!request) {
      const error = new Error("Could not find Request.");
      error.statusCode = 404;
      throw error;
    }
    if (request.job.creator.toString() !== req.userId) {
      const error = new Error("Not authorized!");
      error.statusCode = 403;
      throw error;
    }
    await Request.findByIdAndRemove(requestId);
    const user = await User.findById(req.userId);
    user.requests.pull(requestId);
    await user.save();
    res.status(200).json({ message: "Request Declined" });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};
