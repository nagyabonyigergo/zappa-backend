const fs = require('fs');
const path = require('path');
const aws = require('aws-sdk');

const { validationResult } = require('express-validator/check');

const Job = require('../models/job');
const User = require('../models/user');
const { param } = require('../routes/auth');

exports.getUserJobAndPosts = async (req, res, next) => {
  const userId = req.userId;
  try {
    const jobPostsAndWorkingOn = await User.findById(userId)
      .populate({ path: 'jobposts', populate: { path: 'creator' } })
      .populate({
        path: 'workingon',
        populate: [
          { path: 'creator' },
          { path: 'job', populate: { path: 'creator' } },
        ],
      });
    res.status(200).json({
      message: 'Fetched jobs successfully.',
      posts: jobPostsAndWorkingOn.jobposts,
      jobs: jobPostsAndWorkingOn.workingon,
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.createJob = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    console.log('asdasdasdasd', errors);
    const error = new Error('Validation failed, entered data is incorrect.');
    error.statusCode = 422;
    throw error;
  }
  aws.config.setPromisesDependency();
  aws.config.update({
    accessKeyId: 'AKIAJUHGWAET7QTJIOZQ',
    secretAccessKey: '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',
    region: 'eu-central-1',
  });
  const s3 = new aws.S3();
  const params = {
    ACL: 'public-read',
    Bucket: 'zappa-szakdoga',
    Body: fs.createReadStream(req.file.path),
    Key: req.file.filename,
  };
  s3.upload(params, async(err, data) => {
    if (err) {
      console.log(err);
    }
    if (data) {
      fs.unlinkSync(req.file.path);
      const imageUrl = data.Location;
      const title = req.body.title;
      const description = req.body.description;
      const price = req.body.price;
      const status = req.body.status;
      const category = req.body.category;
      const subcategory = req.body.subcategory;
      const jobTitle = req.body.jobTitle;
      const job = new Job({
        title: title,
        description: description,
        price: price,
        status: status,
        category: category,
        subcategory: subcategory,
        imageUrl: imageUrl,
        jobTitle: jobTitle,
        creator: req.userId,
      });
      try {
        await job.save();
        const user = await User.findById(req.userId);
        user.jobposts.push(job);
        await user.save();
        res.status(201).json({
          message: 'Job created successfully!',
          job: job,
          creator: { _id: user._id, name: user.username },
        });
      } catch (err) {
        if (!err.statusCode) {
          err.statusCode = 500;
        }
        next(err);
      }
    }
  });
 
};

exports.getAllJob = async (req, res, next) => {
  const { price, status, category, subCategory } = req.body;
  let query = {};
  if (price === '50') {
    query.price = { $lte: 50 };
  }
  if (price === '75') {
    query.price = { $gt: 50 };
  }
  if (status != undefined) {
    query.status = status;
  }
  if (category) {
    query.category = category;
  }
  if (subCategory) {
    query.subcategory = subCategory;
  }
  try {
    const jobs = await Job.find(query).populate('creator');
    res.status(200).json({
      message: 'Fetched jobs successfully.',
      jobs: jobs,
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.getJob = async (req, res, next) => {
  const jobId = req.params.id;
  const job = await Job.findById(jobId).populate('creator');
  try {
    if (!job) {
      const error = new Error('Could not find job.');
      error.statusCode = 404;
      throw error;
    }
    res.status(200).json({ message: 'job fetched.', job: job });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.deleteJob = async (req, res, next) => {
  const jobId = req.params.id;
  try {
    const job = await Job.findById(jobId);

    if (!job) {
      const error = new Error('Could not find job.');
      error.statusCode = 404;
      throw error;
    }
    if (job.creator.toString() !== req.userId) {
      const error = new Error('Not authorized!');
      error.statusCode = 403;
      throw error;
    }
    clearImage(job.imageUrl);
    await Job.findByIdAndRemove(jobId);

    const user = await User.findById(req.userId);
    user.jobposts.pull(jobId);
    await user.save();

    res.status(200).json({ message: 'Job Deleted' });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

const clearImage = (filePath) => {
  filePath = path.join(__dirname, '..', filePath);
  fs.unlink(filePath, (err) => console.log(err));
};
