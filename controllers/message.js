const User = require("../models/user");
const Message = require("../models/message");

exports.getMessages = async (req, res, next) => {
  const userId = req.userId;
  try {
    const user = await User.findById(userId).populate({ path: "messages", populate: { path: "creator" } });
    res.status(200).json({
      message: "Fetched request successfully.",
      messages: user.messages,
    }); 
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.createMessages = async (req, res, next) => {
    const messagetToSend = req.body.message;
    const requestId = req.body.requestId;
    const requestCreatorId = req.body.requestCreatorId;
    const message = new Message({
      creator: req.userId,
      message: messagetToSend
    });
    try {
      await message.save();
      const requestCreator = await User.findById(requestCreatorId);
      const messageSentBy = await User.findById(req.userId)
      await requestCreator.messages.push(message);
      await messageSentBy.workingon.push(requestId);
      await messageSentBy.requests.pull(requestId);
      await requestCreator.save();
      await messageSentBy.save();
      res.status(201).json({
        message: "Message sent successfully!"
      }); 
    } catch (err) {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    }
  };

  exports.deleteMessage = async (req, res, next) => {
    const messageId = req.body.messageId;
    try {
      const message = await  Message.findById(messageId).populate("creator");
      if (!message) {
        const error = new Error("Could not find Message.");
        error.statusCode = 404;
        throw error;
      }
      await Message.findByIdAndRemove(message);
  
      const user = await User.findById(req.userId);
      user.messages.pull(messageId);
      await user.save();
  
      res.status(200).json({ message: "Message Deleted" });
    } catch (err) {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    }
  };
  