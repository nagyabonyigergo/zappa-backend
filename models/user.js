const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const userSchema = new Schema({
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  imageUrl: {
    type: String,
    required: false
  },
  messages:[{
    type: Schema.Types.ObjectId,
    ref: 'Message'
  }],
  requests:[{
    type: Schema.Types.ObjectId,
    ref: 'Request'
  }],
  jobposts: [{
    type: Schema.Types.ObjectId,
    ref: 'Job'
  }],
  workingon: [{
    type: Schema.Types.ObjectId,
    ref: 'Request'
  }]
});

module.exports = mongoose.model('User', userSchema);