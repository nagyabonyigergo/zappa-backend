const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const requestSchema = new Schema({
    creator: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    job: {
        type: Schema.Types.ObjectId,
        ref: 'Job'
    }
  }, { timestamps: true });
  
  module.exports = mongoose.model('Request', requestSchema);