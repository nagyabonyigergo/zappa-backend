const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const jobSchema = new Schema(
  {
    title: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: true
    },
    imageUrl: {
      type: String,
      required: true
    },
    creator: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    jobTitle:{
      type: String,
      require: true
    },
    price: {
      type: String,
      required: true
    },
    status: {
      type: Boolean,
      required: true
    },
    hired: [{
      type: Schema.Types.ObjectId,
      ref: 'User'
    }],
    category: {
      type: String,
      required: true
    },
    subcategory: {
      type: String,
      required: true
    }
  }, { timestamps: true }
);


module.exports = mongoose.model('Job', jobSchema);