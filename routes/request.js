const express = require("express");

const requestController = require("../controllers/request");
const isAuth = require("../middleware/is-auth");

const router = express.Router();

router.get("/getrequests", isAuth,  requestController.getRequests);
router.post("/createrequest", isAuth,  requestController.createRequest);
router.get('/getrequestbyid/:id', requestController.getRequestById);
router.delete('/deleteRequest/:id', isAuth, requestController.deleteRequest);

module.exports = router;
