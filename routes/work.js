const express = require("express");
const { body } = require("express-validator");

const workController = require("../controllers/work");
const isAuth = require("../middleware/is-auth");

const router = express.Router();

router.get("/getuserjobandposts", isAuth,  workController.getUserJobAndPosts);

router.post(
  "/createjob",
  isAuth,
  workController.createJob
);

router.post("/getalljob",  workController.getAllJob);

router.get('/getjob/:id', workController.getJob);

router.delete('/deletejob/:id', isAuth, workController.deleteJob);

module.exports = router;
