const express = require("express");

const messageController = require("../controllers/message");
const isAuth = require("../middleware/is-auth");

const router = express.Router();

router.get("/getmessages", isAuth,  messageController.getMessages);
router.post("/createmessage", isAuth,  messageController.createMessages);
router.post('/deletemessage', isAuth, messageController.deleteMessage);

module.exports = router;
