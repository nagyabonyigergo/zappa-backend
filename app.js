const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const multer = require('multer');
const helmet = require('helmet');

const app = express();
const authRoutes = require('./routes/auth');
const workRoutes = require('./routes/work');
const requestRoutes = require('./routes/request');
const messageRoutes = require('./routes/messages');

app.use(helmet());

//Image handling
 const fileStorage = multer.diskStorage({
  filename: (req, file, cb) => {
    cb(null, new Date().toISOString() + '-' + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === 'image/png' ||
    file.mimetype === 'image/jpg' ||
    file.mimetype === 'image/jpeg'
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
}; 

//Middelwares
app.use(bodyParser.json());
 app.use(
  multer({ storage: fileStorage, fileFilter: fileFilter }).single('image')
);
//app.use('/images', express.static(path.join(__dirname, 'images'))); 

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});

//routes
app.use('/auth', authRoutes);
app.use('/work', workRoutes);
app.use('/request', requestRoutes);
app.use('/message', messageRoutes);



//For error handling
app.use((error, req, res, next) => {
  console.log(error);
  const status = error.statusCode || 500;
  const message = error.message;
  res.status(status).json({ message: message });
});


//Connecting to database
mongoose
  .connect(
    `mongodb+srv://gergo:i8Nl45PAGUWU22Fr@cluster0-nrawb.mongodb.net/zappa`,
    { useNewUrlParser: true }
  )
  .then(result => {
    app.listen( process.env.PORT|| 8080);
  })
  .catch(err => console.log(err));
